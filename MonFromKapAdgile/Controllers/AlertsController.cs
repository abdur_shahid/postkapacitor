﻿using Microsoft.ServiceBus.Messaging;
using MonitoringToServiceBus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MonFromKapAdgile.Controllers
{
    public class AlertsController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public string Post([FromBody]object alertsMessage)
        {
            var q = new List<string>();
            var blob = Newtonsoft.Json.JsonConvert.DeserializeObject<AlertMessageModel>(alertsMessage.ToString());
            string message = "";

            try
            {
                foreach (var series in blob.data.series)
                {
                    //message = blob.id + ": " + series.tags.instance + " Drive on " + series.tags.host + " is " + blob.level;
                    BrokeredMessage BM = new BrokeredMessage();
                    BM.MessageId = (blob.id);
                    BM.Properties.Add("QueueName", "Disk_Critical");
                    BM.Properties.Add("QueueAction", "CleanUp");
                    BM.Properties.Add("Instance", series.tags.instance);
                    BM.Properties.Add("Host", series.tags.host);
                    BM.Properties.Add("Level", blob.level);
                    BM.Properties.Add("Full", blob.message.ToString());
                    Adgile.Queues.Queue(series.tags.host).Send<BrokeredMessage>(BM);
                    
                    
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return "Ok";
        }
        void Check()
        {
            BrokeredMessage BM = new BrokeredMessage();
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}