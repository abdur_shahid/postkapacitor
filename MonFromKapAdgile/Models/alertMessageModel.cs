﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MonitoringToServiceBus
{
    public class AlertMessageModel
    {
        public string id { get; set; }
        public string message { get; set; }
        public string details { get; set; }
        public DateTime time { get; set; }
        public double duration { get; set; }
        public string level { get; set; }
        public Data data { get; set; }
        public string previousLevel { get; set; }
        public string recoverable { get; set; }

        public class Data
        {
            public Series[] series { get; set; }


            public class Series
            {
                public string name { get; set; }
                public Tags tags { get; set; }
                public Column column { get; set; }

                public class Tags
                {
                    public string host { get; set; }
                    public string instance { get; set; }
                    public string objectname { get; set; }
                    public string type { get; set; }
                }

                public class Column
                {
                    public DateTime time { get; set; }
                    public float value { get; set; }

                }
            }
        }
    }


}

